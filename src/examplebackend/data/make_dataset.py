
def make_dataset(backend, labels_expected=None):
    """
    This function can convert the input data depending on their format.

    All input files should be read from `data/raw`.
    All generated files should be written to `data/interim`.

    The `make_dataset.py` script is optional, and the corresponding step is
    skipped if the script is not found.
    """
    print("hello world!")
    # scan for data and label files in data/raw:
    files = backend.list_input_files()
    for file in files:
        print(file)


from taggingbackends.main import main

if __name__ == "__main__":
    main(make_dataset)

