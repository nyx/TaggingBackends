
def build_features(backend):
    """
    This function can convert the input data depending on their format.

    Input data files should be read from `data/raw` or `data/interim`.
    Feature files should be written to `data/interim` or `data/processed`.

    The `build_features.py` script is optional, and the corresponding step is
    skipped if the script is not found.
    """
    print("hello world!")


from taggingbackends.main import main

if __name__ == "__main__":
    main(build_features)

