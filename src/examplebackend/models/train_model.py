from taggingbackends.labels import Labels
from mymodel import RandomPredictor

def train_model(backend):
    """
    This function trains a model that can be stored in `backend.model_dir()`.

    The input files can be read from `data/raw` or `data/interim`, depending
    on `make_dataset.py` and `build_features.py`.

    The `train_model.py` script is optional and training is considered as an
    unavailable feature if the script cannot be found.
    """
    # here we expect a `labels.json` file and do not read any data or feature
    # file
    files = backend.list_input_files()
    for file in files:
        if file.name == "labels.json":
            input_labels = Labels().load(file)
        else:
            continue
    # list all the different labels and store the list
    valid_labels = set()
    for run, larva in input_labels:
        timeseries = input_labels[run, larva]
        for timestamp in timeseries:
            valid_labels.add(timeseries[timestamp])
    model = RandomPredictor(list(valid_labels))
    model.dump(backend.model_dir() / "random.json")


from taggingbackends.main import main

if __name__ == "__main__":
    main(train_model)

