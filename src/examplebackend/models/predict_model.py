from taggingbackends.io.trxmat import TrxMat
from taggingbackends.labels import Labels
from mymodel import RandomPredictor

def predict_model(backend):
    """
    This function generates predicted labels for all the input data.

    The input files can be read from any directory.
    All generated/modified files should be written to `data/interim` or
    `data/processed`.
    The predicted labels are expected in `data/processed`.

    The `predict_model.py` script is required.
    """
    # in the present case, as make_dataset.py and build_features.py do nothing,
    # we pick files in `data/raw`
    input_files = backend.list_input_files()
    # we could go and pick files in `data/interim` as well:
    input_files += backend.list_interim_files()
    for file in input_files:
        # load the input data (or features)
        if file.name == "trx.mat":
            timestamps = TrxMat(file)["t"]
        else:
            # TODO: support more file formats
            continue
        # load the model; here, just a list of possible labels
        model_files = backend.list_model_files()
        assert len(model_files) == 1
        model = RandomPredictor().load(model_files[0])
        # assign random labels
        labels = Labels()
        for run in timestamps:
            for larva in timestamps[run]:
                features = timestamps[run][larva]
                labels[run, larva] = model.predict(features)
        # save the predicted labels to file
        labels.dump(backend.processed_data_dir() / "labels.json")


from taggingbackends.main import main

if __name__ == "__main__":
    main(predict_model)

