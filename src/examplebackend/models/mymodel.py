import os
import pathlib
import json
import random

class RandomPredictor:
    def __init__(self, labels=[], seed=None):
        self.valid_labels = labels
        self._seed = seed

    @property
    def seed(self):
        if self._seed is None:
            import sys
            self._seed = random.randrange(sys.maxsize)
        return self._seed

    @seed.setter
    def seed(self, s):
        self._seed = s

    def predict(self, single_larva_timestamps):
        generator = random.Random(self.seed)
        labels = {}
        for timestamp in single_larva_timestamps:
            labels[timestamp] = generator.choice(self.valid_labels)
        return labels

    def _with_file(self, file, mode, fun, *args, **kwargs):
        if isinstance(file, str):
            file = pathlib.Path(file)
        if isinstance(file, pathlib.Path):
            with file.open(mode) as fp:
                return fun(*args, fp, **kwargs)
        else:
            return fun(*args, fp, **kwargs)

    def load(self, file):
        """
        Read the list of labels from file.
        """
        new_self = self._with_file(file, 'r', json.load, cls=RandomPredictorDecoder)
        self.valid_labels = new_self.valid_labels
        self.seed = new_self.seed
        return self

    def dump(self, file):
        """
        Write the list of labels to file.
        """
        self._with_file(file, 'w', json.dump, self, cls=RandomPredictorEncoder)

class RandomPredictorEncoder(json.JSONEncoder):
    def default(self, model):
        if isinstance(model, RandomPredictor):
            data = {}
            if model.valid_labels:
                data["valid_labels"] = model.valid_labels
            if model.seed is not None:
                data["seed"] = model.seed
            return data
        return json.JSONEncoder.default(self, model)

class RandomPredictorDecoder(json.JSONDecoder):
    def decode(self, s):
        data = json.JSONDecoder.decode(self, s)
        labels = data.get("valid_labels", [])
        seed = data.get("seed", None)
        return RandomPredictor(labels, seed)

