module LarvaDatasets

""" Generate *larva_dataset* hdf5 files as introduced by MaggotUBA.

This module exposes the default implementation and proceeds in 3 steps:

  * inspects the data repository for files with labelled spine data,
  * counts the occurrences of each discrete behavior label/tag,
  * defines samples of time segments for each of the specified labels/tags,
  * extracts these time segments and writes them into a HDF5 file.

The main entry point that implements all these steps is [`write_larva_dataset_hdf5`](@ref).

For t5/t15 on hecatonchire, due to the combined overhead of a network connection and the
structure of *trx.mat* files, an alternative implementation is provided by module
`Trxmat2HDF5.jl`.
"""

using PlanarLarvae, PlanarLarvae.Formats, PlanarLarvae.Features, PlanarLarvae.MWT,
      PlanarLarvae.Dataloaders
using PlanarLarvae.Datasets: coerce
using Random
using HDF5
using Dates
using Statistics
using Memoization
using OrderedCollections

export write_larva_dataset_hdf5, first_stimulus, labelcounts, check_larva_dataset_hdf5

"""
    labelcounts(files)
    labelcounts(files, headlength, taillength)
    labelcounts(...; selection_rule, unload)
    labelcounts(files, timebefore::AbstractFloat, timeafter::AbstractFloat; unload::Bool=false)

Count the occurences of all labels, optionally skipping the first `headlength` and last
`taillength` time points of each track.

Return two dictionaries, with labels as keys. The first dictionary gives the corresponding
counts (`eltype` is `Int`), the second one gives all pointers to the occurences.
A pointer is a 3-element tuple: file index, track index, central time point index (all
`Int`).

`selection_rule` is a boolean function that takes a file path (`String`), a track id
(`UInt16`), a timestamp (`Float64`) and the active behavior tags (`BehaviorTags` or `String`
or `Vector{String}`).
It returns `true` for labels to be accounted for, and `false` for those to be skipped.

If `unload=true`, data are unloaded from memory as soon as the corresponding file have been
read.

!!! note

    this function typically is the main bottleneck in the process of generating
    *larva_dataset* hdf5 files. Consider passing `distributed_sampling=true` to
    `write_larva_dataset_hdf5` instead for large databases.

"""
function labelcounts(files, headlength=nothing, taillength=nothing;
        selection_rule=nothing, unload=false)
    if isnothing(headlength)
        headlength = isnothing(taillength) ? 0 : taillength
    end
    if isnothing(taillength)
        taillength = headlength
    end
    ch = Channel(; spawn=true) do ch
        foreach(f -> put!(ch, f), enumerate(files))
    end
    c = Threads.Condition()
    counts = Dict{String, Int}()
    refs = Dict{String, Vector{Tuple{Int, Int, Int}}}()
    Threads.foreach(ch) do (i, file)
        @info "Counting behavior labels in run: $(runid(file))"
        if file isa Formats.JSONLabels
            run = try
                getrun(file)
            catch e
                @error "Failed to load file" file exception=e
                return
            end
            for (j, track) in enumerate(values(run))
                timestamps = track.timestamps
                track = track[:labels]
                lock(c)
                try
                    for k in headlength+1:length(track)-taillength
                        t = timestamps[k]
                        tags = track[k]
                        isnothing(selection_rule) || selection_rule(file.source, track.id, t, tags) || continue
                        for label in (tags isa Vector ? tags : [tags])
                            label = string(label)
                            count = get(counts, label, 0)
                            counts[label] = count + 1
                            ref = get!(refs, label, Tuple{Int, Int, Int}[])
                            push!(ref, (i, j, k))
                        end
                    end
                finally
                    unlock(c)
                end
            end
        elseif file isa Formats.Trxmat
            tracks = try
                gettimeseries(file)
            catch e
                @error "Failed to load file" file exception=e
                return
            end
            for (j, trackid) in enumerate(keys(tracks))
                track = tracks[trackid]
                lock(c)
                try
                    for k in headlength+1:length(track)-taillength
                        t, state = track[k]
                        tags = state[:tags]
                        isnothing(selection_rule) || selection_rule(file.source, trackid, t, tags) || continue
                        for label in convert(Vector{String}, tags)
                            count = get(counts, label, 0)
                            counts[label] = count + 1
                            ref = get!(refs, label, Tuple{Int, Int, Int}[])
                            push!(ref, (i, j, k))
                        end
                    end
                finally
                    unlock(c)
                end
            end
        end
        if unload
            empty!(file.timeseries)
            empty!(file.run)
        end
    end
    return counts, refs
end

function labelcounts(files, timebefore::AbstractFloat, timeafter::AbstractFloat;
        selection_rule=nothing, unload=false)
    ch = Channel(; spawn=true) do ch
        foreach(f -> put!(ch, f), enumerate(files))
    end
    c = Threads.Condition()
    counts = Dict{String, Int}()
    refs = Dict{String, Vector{Tuple{Int, Int, Int}}}()
    Threads.foreach(ch) do (i, file)
        @info "Counting behavior labels in run: $(runid(file))"
        if file isa Formats.JSONLabels
            run = try
                getrun(file)
            catch e
                @error "Failed to load file" file exception=e
                return
            end
            for (j, track) in enumerate(values(run))
                track = track[:labels]
                lock(c)
                try
                    tmin, tmax = track.timestamps[1], track.timestamps[end]
                    for k in 2:length(track)-1
                        t = track.timestamps[k]
                        (tmin <= t - timebefore && t + timeafter <= tmax) || continue
                        tags = track[k]
                        isnothing(selection_rule) || selection_rule(file.source, track.id, t, tags) || continue
                        for label in (tags isa Vector ? tags : [tags])
                            label = string(label)
                            count = get(counts, label, 0)
                            counts[label] = count + 1
                            ref = get!(refs, label, Tuple{Int, Int, Int}[])
                            push!(ref, (i, j, k))
                        end
                    end
                finally
                    unlock(c)
                end
            end
        elseif file isa Formats.Trxmat
            tracks = try
                gettimeseries(file)
            catch e
                @error "Failed to load file" file exception=e
                return
            end
            for (j, trackid) in enumerate(keys(tracks))
                track = tracks[trackid]
                lock(c)
                try
                    tmin, tmax = track[1][1], track[end][1]
                    for k in 2:length(track)-1
                        t, state = track[k]
                        (tmin <= t - timebefore && t + timeafter <= tmax) || continue
                        tags = state[:tags]
                        isnothing(selection_rule) || selection_rule(file.source, trackid, t, tags) || continue
                        for label in convert(Vector{String}, tags)
                            count = get(counts, label, 0)
                            counts[label] = count + 1
                            ref = get!(refs, label, Tuple{Int, Int, Int}[])
                            push!(ref, (i, j, k))
                        end
                    end
                finally
                    unlock(c)
                end
            end
        end
        if unload
            empty!(file.timeseries)
            empty!(file.run)
        end
    end
    return counts, refs
end

"""
    balancedcounts(observed_counts, targetcount=nothing, majorityweight=2)

Derive sample sizes for all labels.

Labels are distinguished in two categories: majority labels and minority labels. Minority
labels share a same sample size, aligned with the least represented label. Majority labels
share another sample size, per default twice as large as that of minority labels.
Argument `majority_weight` defines the sample size ratio between majority and minority
labels.

If `target_count` is set, sample sizes are adjusted so that the total sample size equals this
value. If too few occurences of a label are found, an error is thrown.

See also [`thresholdedcounts`](@ref).
"""
function balancedcounts(counts, targetcount=nothing, majorityweight=2)
    counts = typeof(counts)(k=>count for (k, count) in pairs(counts) if 0 < count)
    majoritythresh = maximum(values(counts)) / 10
    majoritylabels = [k for (k, count) in enumerate(values(counts)) if majoritythresh <= count]
    if isnothing(targetcount)
        basecount = minimum(values(counts))
    else
        basecount = targetcount / (length(counts) + (majorityweight - 1) * length(majoritylabels))
        basecount = round(Int, basecount)
        basecount <= minimum(values(counts)) || throw("cannot provide enough of some label")
    end
    balancedcounts = fill(basecount, (length(counts),))
    for k in majoritylabels
        balancedcounts[k] *= majorityweight
    end
    if !isnothing(targetcount)
        excess = sum(balancedcounts) - targetcount
        if 0 < excess
            balancedcounts[end] -= excess
        elseif excess < 0
            balancedcounts[1] -= excess
        end
    end
    return Dict{eltype(keys(counts)), Int}(zip(keys(counts), balancedcounts)), sum(balancedcounts)
end

"""
    thresholdedcounts(observed_counts, majorityweight=20)

Derive sample sizes for all labels, with an upper bound set as `majorityweight` times
the cardinal of the least represented class.
"""
function thresholdedcounts(counts; majorityweight=20)
    counts = typeof(counts)(k=>count for (k, count) in pairs(counts) if 0 < count)
    majoritythresh = minimum(values(counts)) * majorityweight
    thresholdedcounts = Dict(k=>(count < majoritythresh ? count : majoritythresh) for (k, count) in pairs(counts))
    return thresholdedcounts, sum(values(thresholdedcounts))
end

function makejobs(counts, files, refs::Dict{String, Vector{Tuple{Int, Int, Int}}}; includeall=nothing)
    # this method mutates argument `refs`
    T = eltype(keys(counts))
    refs′= Tuple{Int, Int, Int, T}[]
    if !isnothing(includeall)
        includeall = coerce(T, includeall)
        if haskey(counts, includeall)
            count = counts[includeall]
            T′= Vector{Tuple{Int, Int, Int, T}}
            specialrefs = Dict{T, T′}()
            for (i, j, k) in refs[includeall]
                for l in keys(refs)
                    if l != includeall
                        m = findfirst(==((i, j, k)), refs[l])
                        if !isnothing(m)
                            push!(get!(specialrefs, l, T′()), (i, j, k, l))
                            deleteat!(refs[l], m)
                        end
                    end
                end
            end
            if !isempty(specialrefs)
                @info "Explicit inclusions based on label \"$(includeall)\":" [Symbol(label) => length(refs″) for (label, refs″) in pairs(specialrefs)]...
                for (label, count) in pairs(counts)
                    label == includeall && continue
                    if label in keys(specialrefs)
                        refs″= specialrefs[label]
                        if count < length(refs″)
                            refs″ = shuffle(refs″)[1:count]
                        end
                        refs′= vcat(refs′, refs″)
                        count = count - length(refs″)
                    end
                    if 0 < count
                        for (i, j, k) in shuffle(refs[label])[1:count]
                            push!(refs′, (i, j, k, label))
                        end
                    end
                end
            end
        end
    end
    if isempty(refs′)
        for (label, count) in pairs(counts)
            for (i, j, k) in shuffle(refs[label])[1:count]
                push!(refs′, (i, j, k, label))
            end
        end
    end
    empty!(refs) # free memory
    refs′= sort(refs′)
    sampleid = 0
    return Channel(; spawn=true) do ch
        for (i, file) in enumerate(files)
            refs = [r for (i′, r...) in refs′ if i′==i]
            isempty(refs) && continue
            put!(ch, (file, refs, sampleid))
            sampleid += length(refs)
        end
    end
end

function write_larva_dataset_hdf5(path, counts, files, refs, nsteps_before, nsteps_after;
        fixmwt=false, frameinterval=nothing, includeall=nothing, seed=nothing,
    )
    fixmwt && @warn "`fixmwt=true` is no longer supported"
    @debug "Seeding the random number generator" seed
    isnothing(seed) || Random.seed!(seed)
    ch = makejobs(counts, files, refs; includeall=includeall)
    h5open(path, "w") do h5
        g = create_group(h5, "samples")
        # submit the jobs
        Threads.foreach(ch) do (file, refs, sampleid)
            @info "Sampling series of spines in run: $(runid(file))"
            processfile(g, file, refs, sampleid, nsteps_before, nsteps_after;
                        fixmwt=fixmwt, frameinterval=frameinterval)
        end
        attributes(g)["n_samples"] = sampleid
        # extension
        if !isnothing(includeall)
            delete!(counts, includeall)
        end
        h5["labels"] = collect(keys(counts))
        h5["label_counts"] = collect(values(counts))
        #h5["files"] = [f.source for f in files]
        if !isnothing(frameinterval)
            attributes(g)["frame_interval"] = frameinterval
        end
    end
end

function processfile(h5, file, refs, sampleid, nsteps_before, nsteps_after;
        compat=false, fixmwt=false, unload=true, frameinterval=nothing,
    )
    tracker = genotype_effector = protocol = date_time = filename = timeslot = nothing
    window_length = nsteps_before + nsteps_after + 1
    n = window_length ÷ 3
    mask = compat ? (n+1:2*n) : (1:window_length)
    #
    kwargs = Dict{Symbol, Any}()
    if !isnothing(frameinterval)
        kwargs[:frame_interval] = frameinterval
    end
    #
    tracks = gettimeseries(file; shallow=true)
    median_body_length = medianbodylength(collect(values(tracks)))
    timemapping = nothing
    if Formats.from_mwt(file) && fixmwt
        # supported by the timestamp_recovery branch of PlanarLarvae only
        timemapping = MWT.aligntimesteps(Formats.getnativerepr(file); kwargs...)
    end
    trackids = keys(file isa Formats.JSONLabels ? getrun(file) : tracks)
    for (j, trackid) in enumerate(trackids)
        track′= track = PlanarLarvae.LarvaBase.map′(spine, tracks[trackid])
        timestamps′= timestamps = PlanarLarvae.times(track)
        #
        if Formats.from_mwt(file) && !isnothing(timemapping)
            # supported by the timestamp_recovery branch of PlanarLarvae only
            track′= fixmwtdata(track, timemapping; kwargs...)
            timestamps′= PlanarLarvae.times(track′)
        end
        #
        for (j′, k, label) in refs
            j′==j || continue
            #
            if Formats.from_mwt(file)
                t = timestamps[k]
                if isnothing(timemapping)
                    track′= fixmwtdata(track, anchor_time=t, nframes=window_length; kwargs...)
                    timestamps′= PlanarLarvae.times(track′)
                    k = searchsortedfirst(timestamps′, t)
                    @assert timestamps′[k] == t
                else
                    t′, _ = timemapping[t]
                    k = searchsortedfirst(timestamps′, t′)
                    @assert timestamps′[k] == t′
                end
            end
            #
            start_point = k - nsteps_before
            timeseries = try
                track′[start_point:k+nsteps_after]
            catch
                if isnothing(timemapping)
                    @warn "Interpolated signal contains fewer points than original" start_point
                    if start_point < 1
                        missing_start = 1 - start_point
                        vcat(fill(track′[1], missing_start), track′[1:k+nsteps_after])
                    else
                        missing_stop = k + nsteps_after - length(track′)
                        vcat(track′[start_point:end], fill(track′[end], missing_stop))
                    end
                else
                    rethrow()
                end
            end
            @assert length(timeseries) == window_length
            times, states = [s[1] for s in timeseries], [s[2] for s in timeseries]
            #
            lengths = bodylength.(states)
            spines = spine5.(states)
            spines = normalize_spines(spines, median_body_length; mask=mask)
            #
            sample = zeros(window_length, 18)
            sample[:,1] = times
            sample[:,8] = lengths
            sample[:,9:end] = spines
            #
            name = "sample_$sampleid"
            # transpose for compatibility with h5py
            # see issue https://github.com/JuliaIO/HDF5.jl/issues/785
            h5[name] = permutedims(sample, reverse(1:ndims(sample)))
            sampleid += 1
            #
            d = h5[name]
            isnothing(tracker) || (attributes(d)["tracker"] = tracker)
            isnothing(genotype_effector) || (attributes(d)["line"] = genotype_effector)
            isnothing(protocol) || (attributes(d)["protocol"] = protocol)
            isnothing(date_time) || (attributes(d)["datetime"] = date_time)
            attributes(d)["larva_number"] = convert(Int, trackid)
            isnothing(filename) || (attributes(d)["filename"] = filename)
            isnothing(timeslot) || (attributes(d)["timeslot"] = timeslot)
            attributes(d)["start_point"] = start_point
            attributes(d)["behavior"] = string(label)
            attributes(d)["path"] = file.source
        end
    end
    if unload
        unload!(file)
    end
end

function unload!(file::Formats.JSONLabels)
    for dep in file.dependencies
        unload!(dep)
    end
    empty!(file.timeseries)
    empty!(file.run)
end
function unload!(file)
    empty!(file.timeseries)
    empty!(file.run)
end

@memoize parse_filepath(filepath) = extract_metadata_from_filepath(filepath; sort=false)
@memoize function parse_stim(protocol)
    stim = split(protocol, '_')[end]
    (onset, duration, stim...) = split(stim, 's')
    reps, duration = split(duration, 'x')
    return parse(Float64, onset), parse(Float64, duration)
end

function first_stimulus(filepath, larvaid, t, tags)
    metadata = parse_filepath(filepath)
    :protocol in keys(metadata) || return true
    onset, duration = parse_stim(metadata[:protocol])
    return onset <= t <= onset + duration
end

"""
    write_larva_dataset_hdf5(output_directory, input_files, window_length=20)
    write_larva_dataset_hdf5(...; labels=nothing, labelpointers=nothing)
    write_larva_dataset_hdf5(...; sample_size=nothing, balance=true, seed=nothing)
    write_larva_dataset_hdf5(...; chunks=false, shallow=false)
    write_larva_dataset_hdf5(...; file_filter, timestep_filter)

Sample series of 5-point spines from data files and save them in a hdf5 file,
following the structure of a *larva_dataset* hdf5 file as generated by `maggotuba`.

Input data files can be explicitly listed as `input_files` (`Vector{String}`).
Alternatively, `input_files` can be a `String` and point to the root of a directory tree.
In this latter case, all labelled files found in the directory tree are considered for
sampling series of spines.

These files contain a `samples` *group* that gathers `sample_n` datasets with `n` an integer
ranging from `0` to `sample_size` (if given).

Each `sample_n` dataset is a `[3*window_length] * 18` matrix with attributes including
label `behavior` (string), track ID `larva_number` (int), time point index `start_point`
(int) and file path `path` (string).

The matrix is structured as follows:
* each row represent a successive time point (`window_length` *past* points, `window_length`
*present* points and `window_length` *future* points),
* first column is time,
* columns 2 to 7 are zeros (supposedly indicator variables for hard-coded behaviors; not
used),
* column 8 encodes the larval body length,
* columns 9 to 18 encodes the normalized 5-point spine (x1, y1, x2, y2, etc).

Spines are encoded from tail to head.

See also [`labelledfiles`](@ref Main.PlanarLarvae.Formats.labelledfiles) and
[`labelcounts`](@ref) and their `selection_rule` arguments that correspond to `file_filter`
and `timestep_filter` respectively.

`balance` refers to MaggotUBA-like class balancing. See also [`balancedcounts`](@ref).

Note that, if `input_data` lists all files, `labelledfiles` is not called and arguments
`file_filter`, `chunks` and `shallow` are not used.
Similarly, if `labelpointers` is defined, `labelcounts` is not called and argument
`timestep_filter` is not used.

*New in version 0.10*: `includeall` specifies a secondary label for systematic inclusion in
the dataset. The time segments with this secondary label are accounted for under the
associated primary label, prior to applying the balancing rule. If the label specified by
`includeall` is found in `labels`, it is considered as primary and `includeall` is ignored.
Generally speaking, `labels` should not include any secondary label.

Known issue: ASCII-compatible string attributes are ASCII encoded and deserialized as `bytes`
by the *h5py* Python library.
"""
function write_larva_dataset_hdf5(output_dir::String,
        input_data::Union{Repository, String, <:AbstractVector},
        window_length::Int=20;
        labels::Union{Nothing, <:AbstractVector{String}}=nothing,
        labelpointers::Union{Nothing, <:AbstractDict{String, Vector{Tuple{Int, Int, Int}}}}=nothing,
        chunks::Bool=false,
        sample_size=nothing,
        file_filter=nothing,
        timestep_filter=nothing,
        shallow=false,
        balance=true,
        fixmwt=false,
        frameinterval=nothing,
        includeall="edited",
        seed=nothing,
        distributed_sampling=true,
        past_future_extensions=true,
        balancing_strategy=nothing,
        )
    if distributed_sampling
        new_write_larva_dataset_hdf5(output_dir, input_data;
            window_length=window_length,
            labels=labels,
            labelpointers=labelpointers,
            chunks=chunks,
            sample_size=sample_size,
            file_filter=file_filter,
            timestep_filter=timestep_filter,
            shallow=shallow,
            balance=balance,
            fixmwt=fixmwt,
            frameinterval=frameinterval,
            includeall=includeall,
            balancing_strategy=balancing_strategy,
            past_future_extensions=past_future_extensions,
            seed=seed)
    else
        past_future_extensions || throw("not implemented")
        isnothing(balancing_strategy) || throw("not implemented")
        legacy_write_larva_dataset_hdf5(output_dir, input_data, window_length;
            labels=labels,
            labelpointers=labelpointers,
            chunks=chunks,
            sample_size=sample_size,
            file_filter=file_filter,
            timestep_filter=timestep_filter,
            shallow=shallow,
            balance=balance,
            fixmwt=fixmwt,
            frameinterval=frameinterval,
            includeall=includeall,
            seed=seed)
    end
end

function new_write_larva_dataset_hdf5(output_dir, input_data;
        window_length=20,
        labels=nothing,
        file_filter=nothing,
        balance=nothing,
        balancing_strategy=nothing,
        frameinterval=0.1,
        includeall="edited",
        past_future_extensions=true,
        seed=nothing,
        sample_size=nothing,
        kwargs...)
    repo = if input_data isa String
        isnothing(file_filter) ? Repository(input_data) : Repository(input_data, file_filter)
    elseif input_data isa Repository
        input_data
    else
        Repository(repo = pwd(), files = eltype(input_data) === String ? preload.(input_data) : input_data)
    end
    isempty(repo) && throw("no data files found")
    if !isnothing(get(kwargs, :labelpointers, nothing))
        throw("label pointers are not supported with distributed_sampling=true")
    end
    @assert !isnothing(frameinterval)
    window = TimeWindow(window_length * frameinterval, round(Int, 1 / frameinterval);
                        maggotuba_compatibility=past_future_extensions)
    index = if !isnothing(balancing_strategy) && startswith(balancing_strategy, "max:")
        # `includeall` not supported
        maxcount = parse(Int, balancing_strategy[5:end])
        capacitysampling(labels, maxcount; seed=seed)
    else
        if isnothing(balance)
            balance = lowercase(balancing_strategy) == "maggotuba"
        end
        min_max_ratio = balance ? 2 : 20
        if isnothing(includeall)
            ratiobasedsampling(labels, min_max_ratio; seed=seed)
        else
            ratiobasedsampling(labels, min_max_ratio, prioritylabel(includeall); seed=seed)
        end
    end
    loader = DataLoader(repo, window, index)
    try
        buildindex(loader; unload=true)
    catch
        # most likely error message: "collection must be non-empty"
        @error "Most likely cause: no time segments could be isolated"
        rethrow()
    end
    isnothing(sample_size) || samplesize!(index, sample_size)
    total_sample_size = length(index)
    classcounts, _ = Dataloaders.groupby(index.sampler.selectors, index.targetcounts)
    #
    extended_window_length = past_future_extensions ? 3 * window_length : window_length
    date = Dates.format(Dates.now(), "yyyy_mm_dd")
    win = window_length # shorter name to keep next line within the allowed text width
    output_file = "larva_dataset_$(date)_$(win)_$(win)_$(total_sample_size).hdf5"
    output_file = joinpath(output_dir, output_file)
    h5open(output_file, "w") do h5
        g = create_group(h5, "samples")
        sample(loader, :spine) do _, file, counts, segments
            sampleid, nsegments = counts
            @assert length(segments) == nsegments
            tracks = gettimeseries(file; shallow=true)
            median_body_length = medianbodylength(collect(values(tracks)))
            for segment in segments
                trackid = segment.trackid
                anchortime = segment.anchortime
                timeseries = segment.timeseries
                label = segment.class
                #
                @assert length(segment) == window_length + 1
                @assert length(timeseries) == extendedlength(segment) == extended_window_length + 1
                times, states = [s[1] for s in timeseries], [s[2] for s in timeseries]
                #
                lengths = bodylength.(states[1:end-1])
                spines = spine5.(states)
                mask = Dataloaders.indicator(segment.window, segment)
                spines = normalize_spines(spines, median_body_length; mask=mask)
                #
                sample = zeros(extended_window_length, 18)
                sample[:,1] = times[1:end-1]
                sample[:,8] = lengths
                sample[:,9:end] = spines[1:end-1,:]
                #
                name = "sample_$sampleid"
                # transpose for compatibility with h5py
                # see issue https://github.com/JuliaIO/HDF5.jl/issues/785
                g[name] = permutedims(sample, reverse(1:ndims(sample)))
                sampleid += 1
                #
                d = g[name]
                attributes(d)["larva_number"] = convert(Int, trackid)
                # we should set `start_point` instead of `reference_time`, to comply with the
                # original format, but this would not make sense here due to interpolation:
                attributes(d)["reference_time"] = anchortime
                attributes(d)["behavior"] = string(label)
                attributes(d)["path"] = file.source
            end
        end
        attributes(g)["len_traj"] = window_length
        attributes(g)["len_pred"] = window_length
        attributes(g)["n_samples"] = total_sample_size
        # extensions
        counts = Dataloaders.total(index.targetcounts)
        if isnothing(labels)
            h5["labels"] = string.(keys(classcounts))
            h5["label_counts"] = collect(values(classcounts))
        else
            # ensure labels are ordered as provided in input;
            # see https://gitlab.pasteur.fr/nyx/TaggingBackends/-/issues/24
            if labels isa AbstractDict
                labels = string.(keys(labels))
            end
            h5["labels"] = labels
            h5["label_counts"] = [get(classcounts, Symbol(label), 0) for label in labels]
        end
        if !isnothing(frameinterval)
            attributes(g)["frame_interval"] = frameinterval
        end
    end
    return output_file
end

function legacy_write_larva_dataset_hdf5(output_dir::String,
        input_data::Union{String, <:AbstractVector},
        window_length::Int=20;
        labels::Union{Nothing, <:AbstractVector{String}}=nothing,
        labelpointers::Union{Nothing, <:AbstractDict{String, Vector{Tuple{Int, Int, Int}}}}=nothing,
        chunks::Bool=false,
        sample_size=nothing,
        file_filter=nothing,
        timestep_filter=nothing,
        shallow=false,
        balance=true,
        fixmwt=false,
        frameinterval=nothing,
        includeall="edited",
        seed=nothing,
        )
    files = if input_data isa String
        repository = input_data
        labelledfiles(repository, chunks; selection_rule=file_filter, shallow=shallow)
    elseif eltype(input_data) === String
        preload.(input_data)
    else
        input_data
    end
    isempty(files) && throw("no data files found")
    nsteps_before = window_length + (isodd(window_length) ? (window_length - 1) ÷ 2 : window_length ÷ 2)
    nsteps_after = window_length + (isodd(window_length) ? (window_length - 1) ÷ 2 : window_length ÷ 2 - 1)
    if isnothing(labelpointers)
        if isnothing(frameinterval)
            counts, refs = labelcounts(files, nsteps_before, nsteps_after;
                                       selection_rule=timestep_filter, unload=true)
        else
            timebefore = MWT.seconds(nsteps_before * frameinterval)
            timeafter = MWT.seconds(nsteps_after * frameinterval)
            counts, refs = labelcounts(files, timebefore, timeafter;
                                       selection_rule=timestep_filter, unload=true)
        end
    else
        refs = labelpointers
        counts = Dict{String, Int}(label=>length(pointers) for (label, pointers) in pairs(labelpointers))
    end
    if isnothing(labels)
        labels = collect(keys(counts))
        if !isnothing(includeall) && includeall ∈ labels
            labels = [label for label in labels if label != includeall]
        end
    else
        if !isnothing(includeall) && includeall ∈ labels
            includeall = nothing
        end
        labels′= if isnothing(includeall)
            p -> string(p[1]) in labels
        else
            p -> string(p[1]) in labels || string(p[1]) == includeall
        end
        filter!(labels′, counts)
        filter!(labels′, refs)
        isempty(counts) && throw("None of specified labels were found")
    end
    if balance
        sample_sizes, total_sample_size = balancedcounts(counts, sample_size)
    else
        isnothing(sample_size) || @error "Argument sample_size not supported for the specified balancing strategy"
        sample_sizes, total_sample_size = thresholdedcounts(counts)
    end
    # ensure label order is preserved
    labels′= isnothing(includeall) ? labels : push!(collect(labels), includeall)
    sample_sizes = OrderedDict((label => sample_sizes[label]) for label in labels′ if label in keys(counts))
    #
    @info "Sample sizes (observed, selected):" [Symbol(label) => (get(counts, label, 0), get(sample_sizes, label, 0)) for label in labels]...
    date = Dates.format(Dates.now(), "yyyy_mm_dd")
    output_file = joinpath(output_dir, "larva_dataset_$(date)_$(window_length)_$(window_length)_$(total_sample_size).hdf5")
    write_larva_dataset_hdf5(output_file,
                             sample_sizes, files, refs, nsteps_before, nsteps_after;
                             fixmwt=fixmwt, frameinterval=frameinterval,
                             includeall=includeall, seed=seed)

    h5open(output_file, "cw") do h5
        attributes(h5["samples"])["len_traj"] = window_length
        attributes(h5["samples"])["len_pred"] = window_length
    end
    return output_file
end

function write_pointers(dir, pointers; ext=".csv")
    if isdir(dir)
        for path in readdir(dir; join=true, sort=false)
            if isfile(path) && endswith(path, ext)
                rm(path)
            end
        end
    else
        mkdir(dir)
    end
    for (label, ptrs) in pairs(pointers)
        file = join((label, ext))
        path = joinpath(dir, file)
        open(path, "w") do f
            for ptr in ptrs
                write(f, join(string.(ptr), ','), "\n")
            end
        end
    end
end

"""
    read_pointers(directory)

Read pointer files in directory.

With large files, calling this function may saturate the central memory.
"""
function read_pointers(dir; ext=".csv", labels=nothing)
    isdir(dir) || throw(SystemError("no such directory: $dir"))
    pointers = Dict{String, Vector{Tuple{Int, Int, Int}}}()
    for file in readdir(dir; join=false, sort=false)
        label, fext = splitext(file)
        isnothing(labels) || (label in labels) || continue
        path = joinpath(dir, file)
        (isfile(path) && fext == ext) || continue
        @assert label ∉ keys(pointers)
        ptrs = get!(pointers, label, Tuple{Int, Int, Int}[])
        for line in readlines(path)
            i, j, k = split(line, ',')
            ptr = (parse(Int, i), parse(Int, j), parse(Int, k))
            push!(ptrs, ptr)
        end
    end
    return pointers
end

runid(file) = splitpath(file.source)[end-1]

"""
    check_larva_dataset_hdf5(path)

Read the total label counts and return example time points.
"""
function check_larva_dataset_hdf5(path; print=true)
    h5open(path, "r") do h5
        labels = read(h5, "labels")
        labelcounts = read(h5, "label_counts")
        labelcounts = Dict(Symbol(label) => count for (label, count) in zip(labels, labelcounts))
        print && @info "Labels:" pairs(labelcounts)...
        examples = Dict{Symbol, NamedTuple{(:path, :larva_number, :reference_time), Tuple{String, Int, Float64}}}()
        g = h5["samples"]
        for sampleid in 1:read(attributes(g), "n_samples")
            h = g["sample_$sampleid"]
            label = Symbol(read(attributes(h), "behavior"))
            if label ∉ keys(examples)
                examples[label] = (path=read(attributes(h), "path"), larva_number=read(attributes(h), "larva_number"), reference_time=read(attributes(h), "reference_time"))
                @info "$(label) example" examples[label]...
                length(examples) == length(labels) && break
            end
        end
        return labelcounts, examples
    end
end

end
