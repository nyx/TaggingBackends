import h5py
import pathlib
import itertools
import numpy as np
from collections import Counter

"""
Torch-like dataset class for *larva_dataset hdf5* files.
"""
class LarvaDataset:
    def __init__(self, dataset=None, generator=None, subsets=(.8, .1, .1),
                 balancing_strategy=None, class_weights=None):
        self._path = None
        self.generator = generator
        self._full_set = dataset
        self.subset_shares = subsets
        self._training_set = self._validation_set = self._test_set = None
        self._labels = None
        self._encode = True
        self._sample_size = None
        self._mask = slice(0, None)
        self._window_length = None
        self._class_weights = None
        # this attribute was introduced to implement `training_labels`
        self._alt_training_set_loader = None
        if class_weights is None:
            self.weight_classes = isinstance(balancing_strategy, str) and (balancing_strategy.lower() == 'auto')
        else:
            self.class_weights = class_weights

    """
    *h5py.File*: *larva_dataset hdf5* file handler.
    """
    @property
    def full_set(self):
        if not isinstance(self._full_set, h5py.File):
            self._full_set = h5py.File(str(self.path), "r")
        return self._full_set
    """
    *pathlib.Path*: file path.
    """
    @property
    def path(self):
        if self._path is None:
            if isinstance(self._full_set, (str, pathlib.Path)):
                self.path = self._full_set
        return self._path
    @path.setter
    def path(self, p):
        self._path = p if isinstance(p, pathlib.Path) else pathlib.Path(p)
    """
    *list* of *bytes*: Set of distinct labels.

    If the hdf5 file does not feature a top-level `labels` element that lists
    the labels, the fallback labels and their order are:
    RUN, BEND, STOP, HUNCH, BACK, ROLL.
    """
    @property
    def labels(self):
        if self._labels is None:
            try:
                self._labels = list(self.full_set["labels"])
                # note labels are `bytes` here
            except KeyError:
                # MaggotUBA default
                self._labels = ["RUN", "BEND", "STOP", "HUNCH", "BACK", "ROLL"]
                # note labels are `str` here
        return self._labels
    def __len__(self):
        if self._sample_size is None:
            g = self.full_set["samples"]
            try:
                self._sample_size = g.attrs["n_samples"]
            except KeyError:
                self._sample_size = len(g)
            last = self._sample_size - 1
            assert f"sample_{last}" in g
        return self._sample_size
    def __getitem__(self, sampleid):
        record = self.full_set[f"samples/sample_{sampleid}"]
        x = record[...]
        y = record.attrs["behavior"]
        return self.transform(x), self.encode(y)
    """
    Format input data.
    """
    def transform(self, x):
        #print(x.shape)
        assert x.shape[1] == 18
        x = x[self._mask, 8:]
        x = x.reshape((x.shape[0], 5, 2)).transpose(2, 1, 0)
        #print(x.shape)
        assert x.shape[0:-1] == (2, 5)
        return x
    """
    Format output data (labels to predict).
    """
    def encode(self, y):
        if self._encode:
            labels = self.labels
            return labels.index(y) if labels else y
        else:
            return y
    """
    Split the full dataset into a training and validation sets.
    """
    def split(self):
        try:
            self.generator.device
        except AttributeError:
            raise
        else:
            return self.split_with_Torch()
    def split_with_Torch(self):
        import torch
        import torch.utils.data
        class TorchDataset(torch.utils.data.Dataset):
            def __len__(this):
                return self.__len__()
            def __getitem__(this, i):
                return self.__getitem__(i)
        # subset sizes
        ntot = len(self)
        ntrain, nval, ntest = subset_size(ntot, *self.subset_shares)
        # fork the rng (may not be useful at all)
        g_train = torch.Generator(self.generator.device)
        g_train.set_state(self.generator.get_state())
        # borrowed from MaggotUBA
        train, val, test = torch.utils.data.random_split(TorchDataset(),
                [ntrain, nval, ntest],
                generator=self.generator)
        if ntrain == 0:
            self._training_set = self._alt_training_set_loader = False
        else:
            self._training_set = iter(itertools.cycle(
                torch.utils.data.DataLoader(train,
                    batch_size=self.batch_size,
                    shuffle=True,
                    generator=g_train,
                    drop_last=True)))
            self._alt_training_set_loader = \
                torch.utils.data.DataLoader(train, batch_size=ntrain)
        self._validation_set = iter(
            torch.utils.data.DataLoader(val, batch_size=self.batch_size))
        self._test_set = iter(
            torch.utils.data.DataLoader(test, batch_size=self.batch_size))
    """
    Iterator over the training dataset.
    """
    @property
    def training_set(self):
        if self._training_set is False: # not available; don't call split again
            return
        if self._training_set is None:
            self.split()
        return self._training_set
    """
    Iterator over the validation set.
    """
    @property
    def validation_set(self):
        if self._validation_set is None:
            self.split()
        return self._validation_set
    """
    Iterator over the test set.
    """
    @property
    def test_set(self):
        if self._test_set is None:
            self.split()
        return self._test_set
    """
    This property was introduced to implement `class_weights`.
    It does not memoize.
    """
    @property
    def training_labels(self):
        if self._alt_training_set_loader is False: # not available; don't call split again
            return
        if self._alt_training_set_loader is None:
            self.split()
        _, labels = next(iter(self._alt_training_set_loader))
        try:
            return labels.numpy()
        except AttributeError:
            return labels
    """
    Draw an observation.

    Warning: this actually drew a batch, not an observation;
             it now throws an error.
    """
    def getobs(self, subset="train"):
        raise NotImplementedError("renamed getbatch")
    """
    Draw a series of observations (or batch).
    """
    def getbatch(self, subset="train"):
        if subset.startswith("train"):
            dataset = self.training_set
        elif subset.startswith("val"):
            dataset = self.validation_set
        elif subset.startswith("test"):
            dataset = self.test_set
        return next(dataset)
    """
    Draw one or more batches.
    """
    def getsample(self, subset="train", nbatches=1):
        if subset.startswith("train"):
            dataset = self.training_set
        elif subset.startswith("val"):
            dataset = self.validation_set
        elif subset.startswith("test"):
            dataset = self.test_set
        if nbatches == "all":
            if isinstance(dataset, itertools.cycle):
                logging.warning("drawing unlimited number of batches from circular dataset")
                nbatches = np.inf
            else:
                nbatches = len(dataset)
        try:
            while 0 < nbatches:
                nbatches -= 1
                yield next(dataset)
        except StopIteration:
            pass
    """
    *int*: number of time points in a segment.
    """
    @property
    def window_length(self):
        if self._window_length is None:
            allrecords = self.full_set["samples"]
            anyrecord = allrecords[next(iter(allrecords.keys()))]
            self._window_length = anyrecord.shape[0]
        return self._window_length

    @property
    def weight_classes(self):
        return self._class_weights is not False

    @weight_classes.setter
    def weight_classes(self, do_weight):
        if do_weight:
            if self._class_weights is False:
                self._class_weights = True
        else:
            self._class_weights = False

    @property
    def class_weights(self):
        if not isinstance(self._class_weights, np.ndarray) and self._class_weights in (None, True):
            try:
                class_counts = np.asarray(self.full_set["label_counts"])
            except KeyError:
                _, class_counts = np.unique(self.training_labels, return_counts=True)
                class_counts = np.array([class_counts[i] for i in range(len(self.labels))])
            self._class_weights = 1 - class_counts / np.sum(class_counts)
        return None if self._class_weights is False else self._class_weights

    @class_weights.setter
    def class_weights(self, weights):
        if weights not in (None, False):
            weights = np.asarray(weights)
            if len(weights) != len(self.labels):
                raise ValueError("not as many weights as labels")
        self._class_weights = weights

def subset_size(ntot, train_share, val_share, test_share):
    ntrain = int(train_share * ntot)
    nval = int(val_share * ntot)
    ntest = int(test_share * ntot)
    # adjust
    ndelta = ntrain + nval + ntest - ntot
    while ndelta < 0:
        if 0 < val_share:
            nval += 1
            ndelta += 1
        if ndelta < 0:
            if 0 < test_share:
                ntest += 1
                ndelta += 1
            if ndelta < 0:
                ntrain += 1
                ndelta += 1
    while 0 < ndelta:
        if 0 < train_share:
            ntrain -= 1
            ndelta -= 1
        if 0 < ndelta:
            if 0 < test_share:
                ntest -= 1
                ndelta -= 1
            if 0 < ndelta:
                nval -= 1
                ndelta -= 1
    assert 0 <= ntrain and 0 <= nval and 0 <= ntest
    assert ntrain + nval + ntest == ntot
    return ntrain, nval, ntest

