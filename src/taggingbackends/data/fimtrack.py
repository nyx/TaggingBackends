import csv
import numpy as np

def read_spines(file, fps=None):
    with open(file) as f:
        reader = csv.reader(f, delimiter=",")
        it = iter(reader)
        header = next(it)
        larvae = [int(colname.split('(')[1][:-1]) for colname in header[1:]]
        while True:
            row = next(it)
            if row[0].startswith("head_x("):
                break
        t = {}
        data = {larva: [] for larva in larvae}
        row, ftrname, cols_x, cols_y = read_pointseries(it, row)
        for j, larva in enumerate(larvae):
            t[larva] = np.array([i for i, val in enumerate(cols_x[j]) if val != ""])
            data[larva].append(subarray(cols_x[j]))
            data[larva].append(subarray(cols_y[j]))
        while True:
            row, ftrname, cols_x, cols_y = read_pointseries(it, row)
            for j, larva in enumerate(larvae):
                data[larva].append(subarray(cols_x[j]))
                data[larva].append(subarray(cols_y[j]))
            if ftrname == "tail":
                break
    data = {larva: np.stack(data[larva], axis=1) for larva in data}
    # assume time series are 0-indexed
    if fps:
        for larva in t:
            t[larva] = t[larva] / fps
    return t, data

def read_pointseries(it, row):
    ftrname, _ = row[0].split("_x(")
    x = [[] for _ in row[1:]]
    n = len(x)
    while row[0].startswith(ftrname + "_x("):
        for i in range(n):
            x[i].append(row[i+1])
        row = next(it)
    y = [[] for _ in row[1:]]
    while row[0].startswith(ftrname + "_y("):
        for i in range(n):
            y[i].append(row[i+1])
        row = next(it)
    return row, ftrname, x, y

def subarray(list_):
    return np.array([int(val) for val in list_ if val != ""])

