import numpy as np
import pandas as pd

def parse_row(line):
    elems = iter(line.rstrip().split(' '))
    converted_elems = []
    # date_time
    elem = next(elems)
    converted_elems.append(elem)
    # larva_id
    elem = int(next(elems))
    converted_elems.append(elem)
    # time and space coordinates
    while True:
        try:
            elem = float(next(elems))
        except StopIteration:
            break
        else:
            converted_elems.append(elem)
    return converted_elems

def load_row(data_cols, nrows, row):
    elems = iter(row)
    cols = iter(data_cols)
    while True:
        elem = col = None
        try:
            elem = next(elems)
            col = next(cols)
        except StopIteration:
            if elem is None:
                while True:
                    try:
                        col = next(cols)
                    except StopIteration:
                        break
                    else:
                        col.append(np.nan)
            else:
                while True:
                    extra_col = [np.nan] * nrows
                    extra_col.append(elem)
                    data_cols.append(extra_col)
                    try:
                        elem = next(elems)
                    except StopIteration:
                        break
            break
        else:
            col.append(elem)

def to_dataframe(data_cols):
    coord_cols = list(range(len(data_cols)-3))
    col_names = ['date_time', 'larva_id', 'time'] + coord_cols

    data_cols = [data_cols[0]] + [
            np.array(datacol) for datacol in data_cols[1:]
            ]

    return pd.DataFrame({ colname: datacol \
            for colname, datacol in zip(col_names, data_cols) })

def _load_outline(filepath):
    data_cols = []
    nrows = 0
    with open(filepath, 'r') as f:
        for line in f.readlines():
            row = parse_row(line)
            load_row(data_cols, nrows, row)
            nrows += 1

    if not data_cols:
        raise ValueError('no data found')

    return to_dataframe(data_cols)

def read_larvae(file_obj):
    prev_larva_id = None
    read_larva_ids = set()
    for line in file_obj.readlines():
        row = parse_row(line)
        larva_id = row[1]
        if larva_id != prev_larva_id:
            if larva_id in read_larva_ids:
                raise RuntimeError('rows are not grouped by larva')
            read_larva_ids.add(larva_id)
            if prev_larva_id is not None:
                yield to_dataframe(data_cols)
            prev_larva_id = larva_id
            data_cols = []
            nrows = 0
        load_row(data_cols, nrows, row)
        nrows += 1
    if data_cols:
        yield to_dataframe(data_cols)

# save pandas.DataFrame to file

def rtrim_nan(filt):
    rank = np.cumsum(np.flip(np.isnan(filt)))
    rank = rank[rank == np.arange(1, rank.size + 1)]
    stop = rank.size - np.argmax(rank)
    trimmed = filt[:stop]
    return trimmed

def _save_outline(filepath, df, append=False, float_format='.3f'):
    if float_format.startswith('%'):
        float_format = float_format[1:]
    date_time, larva_id = df['date_time'], df['larva_id']
    points = df[df.columns[3:]].values
    with open(filepath, 'a' if append else 'w') as f:
        newline = '' if f.tell()==0 else '\n'
        for date_time, larva_id, points in zip(
                date_time, larva_id, points):
            points = rtrim_nan(points)
            fmt = '{}{} {:d}'+f' {{:{float_format}}}'*(points.size)
            f.write(fmt.format(newline, date_time, larva_id, *points))
            newline = '\n'

def as_outline(filepath):
    if str(filepath).endswith(".spine"):
        try:
            filepath = filepath.with_suffix(".outline")
        except AttributeError:
            filepath = filepath[:-5]+"outline"
    return filepath

def as_spine(filepath):
    if str(filepath).endswith(".outline"):
        try:
            filepath = filepath.with_suffix(".spine")
        except AttributeError:
            filepath = filepath[:-7]+"spine"
    return filepath

def load_outline(filepath):
    return _load_outline(as_outline(filepath))

def load_spine(filepath):
    return _load_outline(as_spine(filepath))

def save_outline(filepath, *args, **kwargs):
    return _save_outline(as_outline(filepath), *args, **kwargs)

def save_spine(filepath, *args, **kwargs):
    return _save_outline(as_spine(filepath), *args, **kwargs)

