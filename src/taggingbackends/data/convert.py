from .trxmat import TrxMat
from .labels import Labels
import numpy as np
from pathlib import Path

"""
Import behavior labels from a trx.mat file and return a Labels object.
"""
def import_labels_from_trxmat(trxmat_file, labels, decode=True):
    if not isinstance(labels, list) or not labels:
        raise ValueError("labels should be a non-empty list of strings")
    if isinstance(trxmat_file, str):
        trxmat_file = Path(trxmat_file)
    trxmat = TrxMat(trxmat_file)
    trxmat_labels = trxmat.read(['t'] + labels)
    imported_labels = Labels(labelspec=labels, tracking=[trxmat_file])
    run = next(iter(trxmat_labels[labels[0]]))
    larvae = list(trxmat_labels[labels[0]][run])
    for larva in larvae:
        times = trxmat_labels['t'][run][larva]
        for i, label in enumerate(labels):
            indicator = (i+1) * (trxmat_labels[label][run][larva]==1)
            if i==0:
                encoded_labels = indicator
            else:
                encoded_labels += indicator
        if np.any(len(labels) < encoded_labels):
            raise NotImplementedError("overlapping labels")
        if decode:
            _labels = imported_labels.decode(encoded_labels)
        else:
            _labels = encoded_labels
        imported_labels[(run, larva)] = {t: l for t, l in zip(times, _labels)}
    return imported_labels

