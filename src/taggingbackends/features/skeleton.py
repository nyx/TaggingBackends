import numpy as np

def get_5point_spines(spine):
    """
    Convert 11-point spines into 5-point spines.

    Supports both single spines as 11x2 arrays and vectorized spines as Nx22
    arrays.
    """
    if isinstance(spine, list):
        return np.vstack([get_5point_spines(s) for s in spine])
    elif spine.shape[1] == 22:
        return np.hstack([spine[:,0:2], (spine[:,4:6]+spine[:,6:8])/2,
            spine[:,10:12], (spine[:,14:16]+spine[:,16:18])/2, spine[:,20:22]])
    elif spine.shape == (11, 2):
        return np.hstack([spine[[0]], (spine[[2]]+spine[[3]])/2, spine[[5]],
            (spine[[7]]+spine[[8]])/2, spine[[10]]])
    elif spine.shape[1] == 10:
        return spine
    else:
        raise NotImplementedError(spine.shape)

def interpolate(times, spines, anchor, window_length,
                spine_interpolation='linear', frame_interval=0.1, **kwargs):
    """
    Interpolate spine series around anchor time `times[anchor]`, with about
    `window_length // 2` time steps before and after, evenly spaced by
    `frame_interval`.

    Only linear interpolation is supported for now.
    """
    # m = anchor
    # n = m + window_length
    # if n <= spines.shape[0]:
    #     return spines[m:n,:]
    # else:
    #     return
    assert spine_interpolation == 'linear'
    tstart, anchor, tstop = times[0], times[anchor], times[-1]
    istart = np.trunc((tstart - anchor) / frame_interval).astype(int)
    istop = np.trunc((tstop - anchor) / frame_interval).astype(int)
    nframes_before = window_length // 2
    nframes_after = window_length - 1 - nframes_before
    istart = max(-nframes_before, istart)
    istop = min(nframes_after, istop)
    if istop - istart + 1 < window_length:
        return
    grid = range(istart, istop+1)
    times = np.around(times, 4)
    series = []
    for i in grid:
        t = np.around(anchor + i * frame_interval, 4)
        # nextafter may be useless now that we use around instead of trunc
        inext = np.flatnonzero(t <= times)[0]
        tnext, xnext = times[inext], spines[inext]
        if tnext - t < 1e-4:
            x = xnext
        else:
            assert 0 < inext
            tprev, xprev = times[inext-1], spines[inext-1]
            x = interp(xprev, xnext, (t - tprev) / (tnext - tprev))
        series.append(x)
    return np.stack(series, axis=0)

def interp(x0, x1, alpha):
    return (1 - alpha) * x0 + alpha * x1
