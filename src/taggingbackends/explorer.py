import os
import sys
import glob
import shutil
import pathlib
import fnmatch
import importlib
import logging
import subprocess
import tempfile
from collections import defaultdict

JULIA_PROJECT = os.environ.get('JULIA_PROJECT', '')
DISABLE_JULIA = os.environ.get('DISABLE_JULIA', '')

try:
    from julia import Julia
except:
    if not DISABLE_JULIA:
        logging.warning(f"PyCall not found in JULIA_PROJECT={JULIA_PROJECT}; \
please see https://gitlab.pasteur.fr/nyx/TaggingBackends#recommended-installation")
else:
    try:
        Julia(compiled_modules=False)
    except:
        if not DISABLE_JULIA:
            logging.warning("Failed to set PyCall with compiled_modules=False")
    try:
        from julia import TaggingBackends
        from julia import PlanarLarvae
    except:
        if not DISABLE_JULIA:
            logging.warning(f"TaggingBackends not found in JULIA_PROJECT={JULIA_PROJECT}; \
please see https://gitlab.pasteur.fr/nyx/TaggingBackends#recommended-installation")

def getlogger(name):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    return logger

def run(cmd):
    return subprocess.run(cmd, capture_output=True)

def check_permissions(file):
    try:
        uid = os.environ['HOST_UID']
        gid = os.environ['HOST_GID']
    except KeyError:
        pass
    else:
        os.chown(str(file), int(uid), int(gid))


class BackendExplorer:
    """
    Locator for paths to data, scripts, model instances, etc.
    """

    def __init__(self, project_dir=None, package_name=None, model_instance=None,
                 sandbox=None):
        self.project_dir = pathlib.Path(os.getcwd() if project_dir is None else project_dir)
        logging.debug(f"project directory: {self.project_dir}")
        self._package_name = package_name
        self._model_instance = model_instance
        # scripts
        self._make_dataset = None
        self._build_features = None
        self._train_model = None
        self._predict_model = None
        self._finetune_model = None
        self._embed_model = None
        #
        self._sandbox = sandbox

    @property
    def package_name(self):
        if self._package_name is None:
            self._package_name = self._locate_package()
        if self._package_name is not False:
            return self._package_name

    def _locate_package(self):
        src = self.project_dir / "src"
        if src.is_dir():
            for candidate_path in src.iterdir():
                if candidate_path.is_dir() and (candidate_path / "__init__.py").is_file():
                    pkgname = candidate_path.parts[-1]
                    if pkgname != "taggingbackends":
                        return pkgname
                    # TODO: check no other packages are defined?
        logging.warning(f"""\
Cannot find any Python package in project root directory:
{self.project_dir}\
""")
        return False

    @property
    def time_format(self):
        return "%y%m%d_%H%M%S"

    @property
    def model_instance(self):
        if self._model_instance is None:
            import time
            self._model_instance = time.strftime(self.time_format)
        return self._model_instance

    def list_model_instances(self):
        models = self.project_dir / "models"
        if models.is_dir():
            return [model.parts[-1] for model in models.iterdir() if model.is_dir()]
        else:
            return []

    @property
    def make_dataset(self):
        if self._make_dataset is None:
            self._make_dataset = self._locate_script("data", "make_dataset")
        if self._make_dataset is not False:
            return self._make_dataset

    @property
    def build_features(self):
        if self._build_features is None:
            self._build_features = self._locate_script("features", "build_features")
        if self._build_features is not False:
            return self._build_features

    @property
    def train_model(self):
        if self._train_model is None:
            self._train_model = self._locate_script("models", "train_model")
        if self._train_model is not False:
            return self._train_model

    @property
    def predict_model(self):
        if self._predict_model is None:
            self._predict_model = self._locate_script("models", "predict_model")
        if self._predict_model is not False:
            return self._predict_model

    @property
    def finetune_model(self):
        if self._finetune_model is None:
            self._finetune_model = self._locate_script("models", "finetune_model")
        if self._finetune_model is not False:
            return self._finetune_model

    @property
    def embed_model(self):
        if self._embed_model is None:
            self._embed_model = self._locate_script("models", "embed_model")
        if self._embed_model is not False:
            return self._embed_model

    def _locate_script(self, subpkg, basename):
        basename = basename + ".py"
        in_root_dir = self.project_dir / basename
        if in_root_dir.is_file():
            return in_root_dir
        in_scripts_dir = self.project_dir / "scripts"
        if in_scripts_dir.is_dir():
            in_scripts_dir = in_scripts_dir / basename
            if in_scripts_dir.is_file():
                return in_scripts_dir
        in_src_dir = self.project_dir / "src" / subpkg / basename
        if in_src_dir.is_file():
            return in_src_dir
        if self.package_name:
            in_src_dir = self.project_dir / "src" / self.package_name / subpkg / basename
            if in_src_dir.is_file():
                return in_src_dir
        return False

    def _parse_stdout(self, logger, lines, line):
        for prefix, log in (('DEBUG:', logger.debug),
                            ('INFO:', logger.info),
                            ('WARNING:', logger.warning),
                            ('ERROR:', logger.error)):
            if line.startswith(prefix):
                if lines:
                    logger.info("\n".join(lines))
                log(line[len(prefix):].lstrip())
                return []
        if line.startswith("  warn(") or line.startswith("  warnings.warn("):
            pass
        else:
            lines.append(line)
        return lines

    def _parse_stderr(self, logger, lines, line):
        if 'could not import HDF5.exists into MAT' in line:
            return
        # Python logging
        for prefix, log in (('DEBUG:', logger.debug),
                            ('INFO:', logger.info),
                            ('WARNING:', logger.warning),
                            ('ERROR:', logger.error)):
            if line.startswith(prefix):
                line = line[len(prefix):]
                if line.startswith("root:"):
                    line = line[5:]
                line = '\n'.join(line.lstrip().split(r'\n'))
                log(line)
                return
        # other Python warning messages from dependencies
        if line.startswith("  warn(") or line.startswith("  warnings.warn("):
            pass
            #logger.warning(line)
        elif ": UserWarning: " in line:
            logger.warning(line)
        # Julia logging
        elif line and line[0] in "[┌│└":
            print(line)
        # Torch logs
        elif line == '  return torch._C._cuda_getDeviceCount() > 0':
            # typically follows:
            # UserWarning: CUDA initialization: CUDA unknown error
            print(line)
        elif line == '  return F.conv2d(input, weight, bias, self.stride,':
            # typically follows:
            # UserWarning: Plan failed with a cudnnException: CUDNN_BACKEND_EXECUTION_PLAN_DESCRIPTOR: cudnnFinalize Descriptor Failed cudnn_status: CUDNN_STATUS_NOT_SUPPORTED ...
            print(line)
        # tensorflow logs
        elif 26 < len(line):
            # assume line[:26] to be e.g.: 2022-05-17 18:48:15.120981
            logmsg = line[26:]
            # tensorflow logs begin with one-letter log level
            if logmsg.startswith(": W "):
                logger.warning(line)
            elif logmsg.startswith(": I "):
                logger.info(line)
            else:
                lines.append(line)
        else:
            lines.append(line)

    def _run_script(self, path, trailing=None, **kwargs):
        if not path:
            return
        cmd = [sys.executable,
                str(path),
                json.dumps(self, cls=BackendExplorerEncoder, separators=(',', ':'))]
        def _encode(val):
            if isinstance(val, bool):
                return "true" if val else "false"
            return val
        for opt in kwargs:
            cmd.extend([opt, _encode(kwargs[opt])])
        getlogger('main').info(f"running {path.name}")
        logger = getlogger(path.stem)
        logger.setLevel(logging.DEBUG)
        if trailing:
            logger.debug(f"trailing arguments: {trailing}")
            for kv in trailing.items():
                cmd.extend(kv)
        ret = run(cmd)
        if ret.stdout:
            msg = ret.stdout.decode("utf-8")
            lines = []
            for line in msg.splitlines():
                lines = self._parse_stdout(logger, lines, line)
            if lines:
                logger.info("\n".join(lines))
        if ret.stderr:
            msg = ret.stderr.decode("utf-8")
            lines = []
            for line in msg.splitlines():
                self._parse_stderr(logger, lines, line)
            if lines:
                msg = f"in {path.name}:\n"+"\n".join(lines)
                raise Exception(msg)
        sys.stderr.flush()
        sys.stdout.flush()
        return ret

    def import_module(self, absolute_name):
        """
        Helper function that prints instructions in the case the requested
        module is not found.

        The following example import statement:

        .. code-block:: python

            import package.module as namespace

        can be written:

        .. code-block:: python

            namespace = backend_explorer.import_module("package.module")

        """
        try:
            pkg = importlib.import_module(absolute_name)
        except ImportError:
            pkg = absolute_name.split('.')[0]
            logging.error(f"""ModuleNotFoundError: \n
Package '{self.package_name}' requires '{pkg}' \n
but this dependency was not properly specified; \n
run `poetry add {pkg}` from directory: \n
{self.project_dir}\
""")
            raise
        return pkg

    @property
    def sandbox(self):
        if self._sandbox is False:
            self._sandbox = None
        elif self._sandbox is True:
            self._sandbox = pathlib.Path(tempfile.mkdtemp(dir=self.project_dir / 'data' / 'raw')).name
            logging.info(f"sandboxing in {self._sandbox}")
        return self._sandbox

    def _model_dir(self, parent_dir, model_instance=None, create_if_missing=True):
        if model_instance is None:
            model_instance = self.model_instance
        data_dir = parent_dir / model_instance
        if create_if_missing:
            data_dir.mkdir(parents=True, exist_ok=True)
        return data_dir

    def raw_data_dir(self, model_instance=None, create_if_missing=True):
        return self._model_dir(
                self.project_dir / "data" / "raw",
                self.sandbox if model_instance is None else model_instance,
                create_if_missing)

    def interim_data_dir(self, model_instance=None, create_if_missing=True):
        return self._model_dir(
                self.project_dir / "data" / "interim",
                self.sandbox if model_instance is None else model_instance,
                create_if_missing)

    def processed_data_dir(self, model_instance=None, create_if_missing=True):
        return self._model_dir(
                self.project_dir / "data" / "processed",
                self.sandbox if model_instance is None else model_instance,
                create_if_missing)

    def model_dir(self, model_instance=None, create_if_missing=True):
        return self._model_dir(
                self.project_dir / "models",
                model_instance,
                create_if_missing)

    def move_to_raw(self, source, copy=True):
        source = pathlib.Path(source)
        output_dir = self.raw_data_dir()
        destination = output_dir / source.name
        destination.parent.mkdir(parents=True, exist_ok=True)
        if not destination.exists() or not destination.samefile(source):
            if copy:
                # Q: can we simply rename the file in the case source and dest are
                #    located on different partitions?
                with destination.open('wb') as f:
                    with source.open('rb') as g:
                        f.write(g.read())
            else:
                destination.unlink(missing_ok=True)
                destination.symlink_to(source.resolve())
            check_permissions(destination)

    def move_to_interim(self, source, destination=None, copy=False):
        """
        Move file(s) from `data/raw` to `data/interim`.

        Data in `data/raw` are immutable. Files "moved" from `data/raw` to
        `data/interim` are not removed from `data/raw` after they can be found
        in `data/interim`.

        Per default, symbolic links are used, unless `copy=True`.

        `source` and `destination` should be relative paths.
        """
        input_dir = self.raw_data_dir()
        output_dir = self.interim_data_dir()
        if source.is_absolute():
            source = source.relative_to(input_dir)
        if destination is None:
            destination = source
        source = input_dir / source
        destination = output_dir / destination
        destination.parent.mkdir(parents=True, exist_ok=True)
        if not destination.exists() or not destination.samefile(source):
            if copy:
                with destination.open('wb') as f:
                    with source.open('rb') as g:
                        f.write(g.read())
            else:
                destination.unlink(missing_ok=True)
                destination.symlink_to(source.resolve())
            check_permissions(destination)

    def move_to_processed(self, source, destination=None, copy=False):
        """
        Move file(s) from `data/interim` to `data/processed`.

        Files in `data/interim` are supposed to be either deleted or moved to
        `data/processed`. If `copy=True`, the file is copied so that the
        original file is left in `data/interim` once it can be found in
        `data/processed`.

        `source` and `destination` should be relative paths.
        """
        input_dir = self.interim_data_dir()
        output_dir = self.processed_data_dir()
        if source.is_absolute():
            source = source.relative_to(input_dir)
        if destination is None:
            destination = source
        source = input_dir / source
        destination = output_dir / destination
        destination.parent.mkdir(parents=True, exist_ok=True)
        # Q: can we simply rename the file in the case source and dest are
        #    located on different partitions?
        if not destination.exists() or not destination.samefile(source):
            with destination.open('wb') as f:
                with source.open('rb') as g:
                    f.write(g.read())
            if not copy:
                source.unlink()
            check_permissions(destination)

    def list_input_files(self, **kwargs):
        return self.list_files(self.raw_data_dir(), **kwargs)

    def list_interim_files(self, **kwargs):
        return self.list_files(self.interim_data_dir(), **kwargs)

    def list_output_files(self, **kwargs):
        return self.list_files(self.processed_data_dir(), **kwargs)

    def list_model_files(self, query=None):
        return self.list_files(self.model_dir(), query)

    def list_files(self, data_dir, query=None, relative=False, group_by_directories=False):
        files = []
        dirs = [data_dir]
        while dirs:
            parent = dirs.pop()
            for child in parent.iterdir():
                if child.is_dir():
                    dirs.append(child)
                elif not query or fnmatch.fnmatch(child.name, query):
                    if relative:
                        child = child.relative_to(data_dir)
                    files.append(child)
        if group_by_directories:
            files_ = defaultdict(list)
            for file in files:
                files_[str(file.parent)].append(file)
            files = dict(files_)
        return files

    def prepare_labels(self, input_files, allowed_file_extensions=None,
        single_input=False):
        if isinstance(input_files, dict):
            input_files_and_labels = dict()
            for parent in input_files.keys():
                input_files_and_labels[parent] = self.prepare_labels(
                    input_files[parent],
                    single_input=single_input,
                    allowed_file_extensions=allowed_file_extensions,
                )
            return input_files_and_labels
        #
        from .data.labels import Labels, labels_file_extension
        labels = Labels()
        # load and remove any *metadata* file.
        # note: these *metadata* files are pushed by LarvaTagger.jl and form an
        #       undocumented mechanism to preserve metadata that are generated
        #       on the Julia side.
        metadata_found = 0
        for file in list(input_files):
            if file.name == "metadata":
                input_files.remove(file)
                with open(file, "r") as f:
                    labels.metadata = json.load(f)
                if metadata_found == 1:
                    logging.warning("multiple metadata files found")
                metadata_found += 1
        # check whether an input data file has been designated
        primary_input = None
        if metadata_found and 'filename' in labels.metadata.keys():
            primary_input = labels.metadata['filename']
            for file in input_files:
                if file.name == primary_input:
                    primary_input = file
                    break
            if isinstance(primary_input, str):
                logging.warning(f'file not found: {primary_input}')
                primary_input = None
        # identify existing labels (label files only)
        labels_found = 0
        if primary_input is None:
            for file in input_files:
                if any(file.name.endswith(ext) for ext in labels_file_extension):
                    labels.input_labels = file
                    if labels_found == 1:
                        logging.warning("multiple label files found")
                    labels_found += 1
        elif any(primary_input.name.endswith(ext) for ext in labels_file_extension):
            labels.input_labels = primary_input
            labels_found = 1
        # filter out likely unsupported files
        if allowed_file_extensions:
            input_files = [f for f in input_files
                if any(f.name.endswith(ext) for ext in allowed_file_extensions)]
        # force-identify the input data files
        assert 0 < len(input_files)
        if single_input and primary_input is None:
            primary_input = input_files[0]
            logging.info(f'selecting file: {primary_input}')
            input_files = [pathlib.Path(f.source)
                for f in PlanarLarvae.Formats.find_associated_files(str(primary_input))]
        # ensure the primary input file comes first in the list of input files
        if not (primary_input is None or input_files[0] == primary_input):
            input_files = [f for f in input_files if f != primary_input]
            input_files.insert(0, primary_input)
        #
        labels.tracking = input_files
        #
        if labels.metadata:
            if labels.metadata.get('date_time', None) == 'NA':
                logging.debug('discarding metadata entry "date_time"')
                del labels.metadata['date_time']
        else:
            file = input_files[0]
            labels.metadata = {'filename': file.name}
        #
        if "software" not in labels.metadata:
            labels.metadata["software"] = {}
        labels.metadata["software"]["tagger"] = dict(
                backend=self.package_name,
                model_instance=self.model_instance)
        #
        return input_files, labels

    def generate_dataset(self, input_files,
            labels=None, window_length=20, sample_size=None, balance=None,
            include_all=None, frame_interval=None, seed=None,
            past_future_extensions=None, balancing_strategy=None):
        """
        Generate a *larva_dataset hdf5* file in data/interim/{instance}/
        """
        if past_future_extensions is None:
            past_future_extensions = True
            logging.warning("Upcoming breaking change: pass argument past_future_extensions=True to BackendExplorer.generate_dataset to maintain current behavior")
        return TaggingBackends.LarvaDatasets.write_larva_dataset_hdf5(
                str(self.interim_data_dir()),
                input_files if isinstance(input_files, list) else str(input_files),
                window_length,
                labels=labels,
                sample_size=sample_size,
                balance=balance,
                includeall=include_all,
                frameinterval=frame_interval,
                balancing_strategy=balancing_strategy,
                past_future_extensions=past_future_extensions,
                seed=seed)

    def compile_trxmat_database(self, input_dir,
            labels=None, window_length=20, sample_size=None, reuse_h5files=False):
        """
        Extract data from *trx.mat* files found below `input_dir`, save
        interim *.h5* data files in data/interim/{instance}/ and generate a
        *larva_dataset hdf5* file similarly to `generate_dataset`.
        """
        logging.warning('BackendExplorer.compile_trxmat_database is deprecated and will soon be removed')
        input_dir = str(input_dir) # in the case input_dir is a pathlib.Path
        interim_dir = str(self.interim_data_dir())
        if not reuse_h5files:
            trxmat = glob.glob(os.path.join(input_dir, "**/trx.mat"),
                    recursive=True)
            TaggingBackends.Trxmat2HDF5.convert_trxmat_to_spineh5(
                    input_dir=input_dir,
                    output_dir=interim_dir,
                    files=trxmat)
        return TaggingBackends.LarvaDatasets.write_larva_dataset_hdf5(
                interim_dir,
                TaggingBackends.Trxmat2HDF5.larvah5files(interim_dir),
                window_length,
                labels=labels,
                sample_size=sample_size)

    def reset_data(self, dir=None, model_instance=None, spare_raw=False, keep_labels=False):
        if dir is None:
            if not spare_raw:
                shutil.rmtree(self.raw_data_dir(model_instance, False), ignore_errors=True)
            shutil.rmtree(self.interim_data_dir(model_instance, False), ignore_errors=True)
            shutil.rmtree(self.processed_data_dir(model_instance, False), ignore_errors=True)
        else:
            met = dict(raw=self.raw_data_dir,
                       interim=self.interim_data_dir,
                       processed=self.processed_data_dir,
                      )[dir]
            shutil.rmtree(met(model_instance, False), ignore_errors=True)

    def reset_model(self, model_instance=None):
        shutil.rmtree(self.model_dir(model_instance, False), ignore_errors=True)

    def reset(self, model_instance=None):
        self.reset_data(model_instance)
        self.reset_model(model_instance)


import json

class BackendExplorerEncoder(json.JSONEncoder):
    def default(self, explorer):
        if isinstance(explorer, BackendExplorer):
            data = {}
            for attr in ("project_dir", "package_name", "model_instance", "sandbox"):
                try:
                    val = getattr(explorer, attr)
                except AttributeError:
                    val = None
                if val is not None:
                    data[attr] = str(val)
            return data
        return json.JSONEncoder.default(self, explorer)

class BackendExplorerDecoder(json.JSONDecoder):
    def decode(self, data):
        return BackendExplorer(**json.JSONDecoder.decode(self, data))

