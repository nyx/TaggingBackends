import pathlib

project_dir = pathlib.Path(__file__).resolve().parent.parent.parent
package_name = pathlib.Path(__file__).parts[-2]
