from taggingbackends import __version__

def test_version():
    assert __version__ == '0.1.0'


import os.path
from .makedata import sample_trxmat_file, sample_trx

class TestDataFiles:
    """
    Test reading data from files.
    """
    def test_fetch_trxmat_file(self, sample_trxmat_file):
        print(sample_trxmat_file)
        assert os.path.isfile(sample_trxmat_file)

    def test_load_trxmat_file(self, sample_trx):
        print(sample_trx.records)
        for record in ("run_large", "cast_large", "back_large", "stop_large",
                "small_motion", "t"):
            assert record in sample_trx.records
        data = sample_trx.read(["t", "spine", "outline"])
        assert not (set(data.keys()) ^ set(("t", "x_spine", "y_spine", "x_contour", "y_contour")))
        print(list(data["t"].keys()))
        assert len(data["t"]) == 1
        run = next(iter(data["t"]))
        assert run == "20150701_105504"
        larvae = data["t"][run]
        print(list(larvae.keys()))
        assert len(larvae) == 182
        #
        t_ = larvae[778]
        assert len(t_) == 236
        assert t_[0] == 99.836 and t_[1] == 99.917 and t_[2] == 99.999
        assert t_[-3] == 119.16 and t_[-2] == 119.266 and t_[-1] == 119.356

    def test_json_labels(self, tmp_path):
        from taggingbackends.labels import Labels
        labels = Labels({
            "20150701_105504": {182: {99.836: "run", 99.917: "bend", 99.999: "stop"}}})
        tmp_file = tmp_path / "labels.json"
        labels.dump(tmp_file)
        labels_ = Labels().load(tmp_file)
        assert labels_.labels == labels.labels


# use `make test_examplebackend` instead
# class TestPrediction:
#     """
#     Test the few taggers shipped with the package.
#     """
#     def test_random_tagger(self, sample_trx):
#         pass

