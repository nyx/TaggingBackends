#!/bin/bash

PROJECT_NAME=$1
PACKAGE_NAME=$2

if [ -z ${PROJECT_NAME} ]; then
echo "expected input argument: backend project name"
exit -1
fi

if [ -z ${PACKAGE_NAME} ]; then
echo "expected input argument: backend package name"
exit -1
fi

cd ..

TRXMAT=$(julia --project=. -e 'include("test/makedata.jl"); print(make_test_data("sample_trxmat_file"))')

rsync -au --exclude=*/.* --exclude=*/__pycache__ --exclude=poetry.lock --exclude=test --exclude=data --exclude=src ../TaggingBackends test
cd test
mv TaggingBackends ${PROJECT_NAME}
cd ${PROJECT_NAME}

sed -i "s/TaggingBackends/${PROJECT_NAME}/" pyproject.toml

# mkdir -p src/${PACKAGE_NAME}/{data,features,models}
# touch src/${PACKAGE_NAME}/__init__.py
# for SRC_DIR in data features models; do
# touch src/${PACKAGE_NAME}/${SRC_DIR}/__init__.py
# done

mkdir -p src/${PACKAGE_NAME}
cp -u ../../src/taggingbackends/info.py src/${PACKAGE_NAME}/

rm -rf $(poetry env info -p)

poetry install
poetry add ../../

mkdir -p data/raw
cp -u ${TRXMAT} data/raw/

