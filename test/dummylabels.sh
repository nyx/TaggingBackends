#!/bin/bash

cat << "EOT" > $1/data/raw/labels.json
{
  "runs": {
    "20150701_105504": {
      "tracks": [
        {
          "id": 512,
          "timestamps": [
            66.675,
            66.755,
            66.856,
            66.949,
            67.039,
            67.115,
            67.196
          ],
          "labels": [
            "crawl",
            "bend",
            "back",
            "stop",
            "undecided",
            "hunch",
	    "roll"
          ]
        }
      ]
    }
  }
}
EOT

