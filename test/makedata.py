from pathlib import Path
project_dir = Path(__file__).parent.parent

import os
os.environ['JULIA_PROJECT'] = str(project_dir)

from julia import Julia
Julia(compiled_modules=False)

from julia import Main
Main.include("test/makedata.jl")

def make_test_data(target="sample_trxmat_file"):
    return Main.make_test_data(f"{target}")

import pytest

@pytest.fixture
def sample_trxmat_file():
    return make_test_data("sample_trxmat_file")

@pytest.fixture
def sample_trx(sample_trxmat_file):
    from taggingbackends.io.trxmat import TrxMat
    return TrxMat(sample_trxmat_file)

