using LazyArtifacts

function make_test_data(target="chore_sample_outline")
    # we want `targets` to be mutable
    isstr = target isa String
    if isstr
        targets = [target]
    else
        # copy
        targets = Vector{String}(target)
    end
    for t in 1:length(targets)
        target = targets[t]
        dir = nothing
        # call the `artifact` string macro
        if startswith(target, "chore_sample_")
            artefact = artifact"chore_sample_output"
            dir = "chore_sample_output"
            filetype = target[length("chore_sample_")+1:end]
            if filetype ∉ ("outline", "spine")
                throw(DomainError("unknown Chore output: " * filetype))
            end
            filename = "20150701_105504@FCF_attP2_1500062@UAS_Chrimson_Venus_X_0070@t15@r_LED50_30s2x15s30s#n#n#n@100." * filetype
        elseif target == "sample_trxmat_file"
            artefact = artifact"sample_trxmat_file"
            filename = "trx.mat"
        else
            throw(DomainError("unknown artefact: " * target))
        end
        # return path to individual file instead
        if isnothing(dir)
            path = joinpath(artefact, filename)
        else
            path = joinpath(artefact, dir, filename)
        end
        targets[t] = path
    end
    if isstr
        targets = targets[1]
    end
    return targets
end
